Features
###############################################################################

:slug: features
:modified: 2016-01-05 14:45

.. _Video streaming: spice-user-manual.html#_video_compression
.. _Image compression: spice-user-manual.html#_image_compression
.. _Two way audio: spice-user-manual.html#_other_features
.. _Windows drivers: spice-user-manual.html#guest-additions
.. _Multiple monitors: spice-user-manual.html#_multiple_monitor_support
.. _Spice agent: spice-user-manual.html#agent
.. _USB redirection: spice-user-manual.html#_usb_redirection
.. _Encrypted communication: spice-user-manual.html#_tls
.. _SASL authentication: spice-user-manual.html#_sasl
.. _Xspice: xspice.html
.. _Smartcard: spice-user-manual.html#_cac_smartcard_redirection
.. _Folder sharing: spice-user-manual.html#_folder_sharing
.. _SSH authentization agent: https://bugs.freedesktop.org/show_bug.cgi?id=85925
.. _Seamless applications: seamless-applications.html
.. _CD sharing: https://bugs.freedesktop.org/show_bug.cgi?id=85956
.. _3D acceleration Support: spice-user-manual.html#_gl_acceleration_virgl
.. _OSX client: osx-client.html
.. _Web client: spice-html5.html
.. _Simultaneous clients connection: multiple-clients.html

Current Features
++++++++++++++++

- `Video streaming`_ - heuristically identifies video streams and transmits M-JPEG
  video streams
- `Image compression`_ - offers various compression algorithm that were built
  specifically for Spice, including QUIC (based on SFALIC), LZ, GLZ
  (history-based global dictionary), and auto (heuristic compression choice per
  image)
- Live migration - supports clients while migrating Spice servers to new hosts,
  thus avoiding interruptions
- `Windows drivers`_ - Windows drivers for QXL display device and VDI-port
- `Multiple monitors`_ with support for enabling selection of the screen used by
  the client (in virt-viewer/remote-viewer)
- `Two way audio`_ - supports audio playback and captures; audio data stream is
  optionally compressed using CELT Encryption - using OpenSSL
- Clipboard sharing and arbitrary resolutions - features provided by the `Spice agent`_ running on the guest
- `USB redirection`_ - allows clients to use their USB devices with Spice servers
- `Encrypted communication`_
- `SASL authentication`_
- Xspice_ - a standalone server that is both an X server and a Spice server
- `Web client`_ - a simple javacript client
- Smartcard_ - CAC smartcard redirection
- `Folder sharing`_ - for sharing a client's folder with the remote guest

Future Features
+++++++++++++++

- `SSH authentization agent`_
- `Seamless applications`_
- `CD sharing`_ - share your CD with Spice server
- `3D acceleration Support`_
- `OSX client`_
- `Simultaneous clients connection`_